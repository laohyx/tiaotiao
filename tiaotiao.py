from pynput import keyboard, mouse
import pyscreenshot as ImageGrab
from multiprocessing import freeze_support
import time
import math
from threading import Thread

def force_sleep(seconds):
	target = time.time() + seconds
	while time.time() < target:
		pass

myMouse = mouse.Controller()
myPosition = (0, 0)
timeScale = 0.72 / 184.56
timeScale3 = 0
centerX = 0
centerY = 0
caliPoints = [(231, 868),
(382, 779),
(532, 692),
(683, 604)]
GRAB_REGION = [0, 0, 0, 0]
SHALL_STOP = False
GRAB_IMAGE = None

def doJump():
	print("jump!")
	dist = calculateProjectedDistance(myPosition, myMouse.position)
	pressTime = timeScale * dist
	print("dist={}, pressTime={}".format(dist, pressTime))
	myMouse.press(mouse.Button.left)
	force_sleep(pressTime)
	myMouse.release(mouse.Button.left)
	print("done")
	return dist
def doJump2():
	print("jump!")
	# dist = calculateProjectedDistance(myPosition, myMouse.position)
	targetX = centerX * 2 - myPosition[0]
	targetY = centerY * 2 - myPosition[1]
	
	myMouse.position = myPosition
	time.sleep(0.5)
	myMouse.position = (int(targetX), int(targetY))
	dist = calculateProjectedDistance(myPosition, (targetX, targetY))
	if dist < 0:
		dist = -dist
	pressTime = timeScale3 * dist

	print("dist={}, pressTime={}".format(dist, pressTime))
	myMouse.press(mouse.Button.left)
	force_sleep(pressTime)
	myMouse.release(mouse.Button.left)
	print("done")
	return pressTime

def keepJumping():
	global SHALL_STOP
	global myPosition
	while not SHALL_STOP:
		myPosition = findMyPosition()
		print(myPosition)
		pressTime = doJump2()
		if pressTime > 0.8:
			time.sleep(3)
		else:
			time.sleep(2)

def euclideanDistance(a, b):
	return math.sqrt((b[1] - a[1]) ** 2 + (b[0] - a[0]) ** 2)
def calculateProjectedDistance(a, b):
	a = [float(x) for x in a]
	b = [float(x) for x in b]
	if b[0] > a[0] and b[1] < a[1]:
		print("right up")
	elif b[0] < a[0] and b[1] < a[1]:
		print("left up")
		# change to right up
		b[0] = 2 * a[0] - b[0]
	else:
		print("error!")
		raise Exception()
	sqrt3 = 3 ** 0.5
	y = (sqrt3 * a[1] - a[0] + sqrt3 * b[1] + b[0]) / 2 / sqrt3
	x = sqrt3 * (b[1] - y) + b[0]
	distance = euclideanDistance(b, (x, y))
	return distance

def findMyPosition():
	global GRAB_IMAGE
	GRAB_IMAGE=ImageGrab.grab(bbox=GRAB_REGION) # X1,Y1,X2,Y2
	cnt = 0
	avgPos = [0, 0]
	# GRAB_IMAGE.show()
	for y in range(GRAB_REGION[3] - GRAB_REGION[1]):
		for x in range(GRAB_REGION[2] - GRAB_REGION[0]):
			px = GRAB_IMAGE.getpixel((x, y))
			if abs(60 - px[0]) < 10 and abs(50 - px[1]) < 10 and abs(90 - px[2]) < 10:
				# myMouse.position = (GRAB_REGION[0] + x, GRAB_REGION[1] + y)
				# force_sleep(0.01)
				# myMouse.press(mouse.Button.left)
				# force_sleep(0.001)
				# myMouse.release(mouse.Button.left)
				avgPos[0] += x
				avgPos[1] += y
				cnt += 1
	avgPos = [int(x / cnt) for x in avgPos]
	# pixel offset
	avgPos[0] += -2
	avgPos[1] += +20
	return (avgPos[0] + GRAB_REGION[0], avgPos[1] + GRAB_REGION[1])

def registerKeyPress():
	def on_press(key):
		return
		try:
			print('alphanumeric key {0} pressed'.format(
				key.char))
		except AttributeError:
			print('special key {0} pressed'.format(
				key))

	def on_release(key):
		global myPosition
		global SHALL_STOP
		try:
			if key == keyboard.Key.esc or key.char == 'q':
				# Stop listener
				SHALL_STOP = True
				return False
			if key.char == '2':
				oldPosition = myPosition
				myPosition = findMyPosition()
				print(myPosition)
				dist = doJump2()
			if key.char == '1':
				oldPosition = myPosition
				myPosition = findMyPosition()
				print(myPosition)
				# log to analyze
				dist = doJump()
			if key.char == '3':
				myMouse.position = findMyPosition()
			if key.char == '0':
				print(myMouse.position)
			if key.char == '4':
				thread = Thread(target = keepJumping)
				thread.start()
		except AttributeError:
			pass

	with keyboard.Listener(
		on_press=on_press,
		on_release=on_release) as listener:
		listener.join()


#####################################
# Calibrate
startX = caliPoints[0][0]
startY = caliPoints[1][1]
endX = caliPoints[2][0]
endY = caliPoints[3][1]
centerX = 0.5 * (startX + endX)
centerY = 0.5 * (startY + endY)
caliDist = calculateProjectedDistance((startX, startY), (endX, endY))
timeScale3 = 0.715 / caliDist
GRAB_REGION[0] = int(startX * 2 - centerX) - 50
GRAB_REGION[1] = int(endY * 2 - centerY) - 50
GRAB_REGION[2] = int(endX * 2 - centerX) + 50
GRAB_REGION[3] = int(startY * 2 - centerY) + 50
# myMouse.position = (GRAB_REGION[0], GRAB_REGION[1])
# time.sleep(1)
# myMouse.position = (GRAB_REGION[2], GRAB_REGION[3])
print("start={}, center={}".format((startX, startY), (centerX, centerY)))
print("caliDist={}, timescale3={}".format(caliDist, timeScale3))
#####################################

if __name__ == '__main__':

	freeze_support()
	registerKeyPress()
